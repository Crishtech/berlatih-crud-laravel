<?php

namespace belajarlaravel\Http\Controllers;

use Illuminate\Http\Request;
use belajarlaravel\Http\Requests;
use belajarlaravel\Http\Controllers\Controller;

class Contoh2 extends Controller {

    public function getIndex() {
        // request GET ke index
        echo 'halaman index';
    }

    public function getShow($id) {
        // mengirim request GET ke 'coba/show/{id}'
        echo 'halaman tampil';
    }

    public function postStore() {
        // mengirim request ke POST 'coba/store'
        echo 'mengirim variable post';
    }

}
